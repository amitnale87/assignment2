package com.pomclasses.pkg;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage
{
	@FindBy(id="tab-flight-tab")
	private WebElement flightsLink;

	@FindBy(id=("flight-origin"))
	private WebElement flyingFromTextField; 

	@FindBy(id=("flight-destination"))
	private WebElement flyingToTextField;

	@FindBy(id=("aria-option-0"))
	private WebElement firstFromCity;

	@FindBy(id=("aria-option-0"))
	private WebElement firstToCity;

	@FindBy (id=("flight-departing"))
	private WebElement departDateTextField;

	@FindBy(xpath=("//div[@id='flight-departing-wrapper']/div/div/div[2]/"
			+ "table/tbody//tr//td/button[@class != 'datepicker-cal-date disabled']"))
	private List <WebElement> activeDeparttureDateList;

	@FindBy (id=("flight-returning"))
	private WebElement ruturnDateTextField;

	@FindBy(xpath=("//div[@id='flight-returning-wrapper']/div/div/div[3]/"
			+ "table/tbody//tr//td/button[@class != 'datepicker-cal-date disabled']"))
	private List <WebElement> activeReturnDateList;

	@FindBy(id=("search-button"))
	private WebElement searchButton;

	WebDriverWait wait=null;

	public HomePage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
		wait=new WebDriverWait(driver, 20);
	}

	public void clickFlights() throws InterruptedException 
	{
		Thread.sleep(500);
		flightsLink.click();

		/*Select From City*/
		flyingFromTextField.click();
		flyingFromTextField.sendKeys("Pune");
		//Thread.sleep(2000);
		//firstFromCity.click();
		wait.until(ExpectedConditions.elementToBeClickable(firstFromCity)).click();


		/*Select To City*/
		flyingToTextField.click();
		flyingToTextField.sendKeys("Mumbai");
		wait.until(ExpectedConditions.elementToBeClickable(firstToCity)).click();


		/*Select Departure Date*/
		departDateTextField.click();
		activeDeparttureDateList.get(0).click();

		/*Select Return Date*/
		ruturnDateTextField.click();
		activeReturnDateList.get(2).click();

	}

	public void bookingExecution() throws InterruptedException
	{
		clickFlights();
	}

	public void searchClick()
	{
		searchButton.click();
	}


}
