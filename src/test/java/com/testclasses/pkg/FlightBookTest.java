package com.testclasses.pkg;

import java.io.IOException;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.baseclass.pkg.Base;
import com.pomclasses.pkg.HomePage;

@Listeners(com.listenerDemo.pkg.ListenerTest.class)
public class FlightBookTest extends Base
{
	HomePage homepage;

	@Test
	public void bookingFlightExecution() throws InterruptedException, IOException
	{
		homepage = new HomePage(driver);
		homepage.bookingExecution();
		takeScreenShot(".\\Screenshots\\BeforeSearch.png");
		homepage.searchClick();
		takeScreenShot(".\\Screenshots\\AfterSearch.png");
	}

}
