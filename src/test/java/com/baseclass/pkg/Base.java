package com.baseclass.pkg;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class Base
{
	public WebDriver driver;
	String chromePath;

	@Parameters("browser")
	@BeforeTest
	public void openBrowser(String browser) {
		if(browser.equals("Chrome"))
		{		
			chromePath = ".\\ChromeBrowserDriver\\chromedriver.exe";
			System.setProperty("webdriver.chrome.driver", chromePath);
			driver = new ChromeDriver();
						
		}
		else if(browser.equals("Firefox"))
		{
			//String firefoxPath = ".\\FirefoxbrowserDriver\\geckodriver.exe";
			//System.setProperty("webdriver.firefox.marionette", firefoxPath);
			//System.setProperty("webdriver.gecko.driver", firefoxPath);
			driver=new FirefoxDriver();
			
			
		}
		else if(browser.equals("MsEdge"))
		{
			String edgeDriverPath = ".\\EdgeBrowserDriver\\MicrosoftWebDriver.exe";
			System.setProperty("webdriver.edge.driver", edgeDriverPath);
			driver=new EdgeDriver();
			
			
		}
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.orbitz.com/");
	}

	@AfterTest
	public void closeBrowser() {
		driver.quit();
	}

	public void takeScreenShot(String filepath) throws IOException {
		TakesScreenshot scrShot = (TakesScreenshot) driver;
		File scrFile = scrShot.getScreenshotAs(OutputType.FILE);
		File desFile = new File(filepath);
		FileUtils.copyFile(scrFile, desFile);
	}

}
